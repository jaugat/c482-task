package app;

/**
 * Supplied class AddProductWindowController.java
 */

/**
 *
 * @author Jordan Augat
 */

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.util.Optional;
import java.util.Random;

public class AddProductWindowController {

    @FXML
    private TextField nameField;

    @FXML
    private TextField invField;

    @FXML
    private TextField priceField;

    @FXML
    private TextField maxField;

    @FXML
    private TextField minField;

    @FXML
    private Label nameError;

    @FXML
    private Label invError;

    @FXML
    private Label priceError;

    @FXML
    private Label minMaxError;

    @FXML
    private TextField allPartsSearchField;

    @FXML
    private TableView<Part> allPartsTable;

    @FXML
    private TableColumn<Part, Integer> allPartsIdColumn;

    @FXML
    private TableColumn<Part, String> allPartsNameColumn;

    @FXML
    private TableColumn<Part, Integer> allPartsInvColumn;

    @FXML
    private TableColumn<Part, Double> allPartsPriceColumn;

    @FXML
    private TableView<Part> associatedPartsTable;

    @FXML
    private TableColumn<Part, Integer> associatedPartsIdColumn;

    @FXML
    private TableColumn<Part, String> associatedPartsNameColumn;

    @FXML
    private TableColumn<Part, Integer> associatedPartsInvColumn;

    @FXML
    private TableColumn<Part, Double> associatedPartsPriceColumn;

    @FXML
    private Label removePartError;

    @FXML
    private Button addPartButton;

    @FXML
    private Button removePartButton;

    @FXML
    private Button createProductButton;

    @FXML
    private Button cancelButton;

    @FXML
    private TextField associatedPartsSearchField;

    private ObservableList<Part> associatedParts = FXCollections.observableArrayList();

    /**
     * Formats both the all parts table and the associated parts table. Gets all parts from inventory and adds them to the all parts table.
     */
    public void initialize () {
        allPartsTable.setItems(Inventory.getAllParts());

        allPartsIdColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        allPartsNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        allPartsInvColumn.setCellValueFactory(new PropertyValueFactory<>("inv"));
        allPartsPriceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));

        associatedPartsIdColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        associatedPartsNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        associatedPartsInvColumn.setCellValueFactory(new PropertyValueFactory<>("inv"));
        associatedPartsPriceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));
    }

    /**
     * Listener for the all parts search field. Filters matching parts or returns an error if no parts are found.
     */
    public void searchAllPartsListener () {
        String str = allPartsSearchField.getText();
        removePartError.setText("");
        ObservableList<Part> temp = FXCollections.observableArrayList();
        if (isInt(str)) {
            for (int i = 0; i < Inventory.getAllParts().size(); i++) {
                if (String.valueOf(Inventory.getAllParts().get(i).getId()).contains(str)) {
                    temp.add(Inventory.getAllParts().get(i));
                }
            }
        } else {
            for (int i = 0; i < Inventory.getAllParts().size(); i++) {
                if ((Inventory.getAllParts().get(i).getName().toLowerCase()).contains(str.toLowerCase())) {
                    temp.add(Inventory.getAllParts().get(i));
                }
            }
        }
        if (temp.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("");
            alert.setHeaderText("Search Error");
            alert.setContentText("No parts found matching input!");

            alert.showAndWait();
        } else {
            allPartsTable.setItems(temp);
        }
    }

    /**
     * Listener for the associated parts search field. Filters matching parts or returns an error if no parts are found.
     * RUNTIME ERROR. Encountered an error in this method where search data was returning parts that had already been removed. If multiple parts were added and you searched for one to remove and removed it, when the table was refreshed the removed part would appear but parts that were not removed disappeared. When printing the search results to the console, I found that the temporary table being used to capture matching items was only referring to a memory location instead of the object for an unknown reason. This was resolved by creating a temporary Part object and casting it as the type of part that was being searched.
     */
    public void searchAssociatedPartsListener () {
        String str = associatedPartsSearchField.getText();
        removePartError.setText("");
        ObservableList<Part> temp = FXCollections.observableArrayList();
        if (isInt(str)) {
            for (int i = 0; i < associatedParts.size(); i++) {
                if (String.valueOf(associatedParts.get(i).getId()).contains(str)) {
                    if (associatedParts.get(i) instanceof OutsourcedPart) {
                        OutsourcedPart newPart = (OutsourcedPart) associatedParts.get(i);
                        temp.add(newPart);
                    } else if (associatedParts.get(i) instanceof InHousePart) {
                        InHousePart newPart = (InHousePart) associatedParts.get(i);
                        temp.add(newPart);
                    }
                }
            }
        } else {
            for (int i = 0; i < associatedParts.size(); i++) {
                if ((associatedParts.get(i).getName().toLowerCase()).contains(str.toLowerCase())) {
                    if (associatedParts.get(i) instanceof OutsourcedPart) {
                        OutsourcedPart newPart = (OutsourcedPart) associatedParts.get(i);
                        temp.add(newPart);
                    } else if (associatedParts.get(i) instanceof InHousePart) {
                        InHousePart newPart = (InHousePart) associatedParts.get(i);
                        temp.add(newPart);
                    }
                }
            }
        }
        if (temp.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("");
            alert.setHeaderText("Search Error");
            alert.setContentText("No parts found matching input!");

            alert.showAndWait();
        } else {
            associatedPartsTable.setItems(temp);
        }
    }

    /**
     * Listener for the add part button. Takes selected item in all parts table and adds it to associated parts.
     */
    public void addPartButtonListener () {
        removePartError.setText("");
        Part selectedItem = allPartsTable.getSelectionModel().getSelectedItem();
        if (associatedParts.contains(selectedItem)) {
            removePartError.setText("Part already added");
        } else if (selectedItem == null) {
            removePartError.setText("No Part Selected");
        } else {
            associatedParts.add(allPartsTable.getSelectionModel().getSelectedItem());
            associatedPartsTable.setItems(associatedParts);
        }
    }

    /**
     * Listener for the remove part button. Opens dialog asking user to confirm they want to remove the part. If confirmed, part is removed from associated parts. If declined, a message appears stating part wasn't removed.
     */
    public void removePartButtonListener () {
        removePartError.setText("");
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("");
        alert.setHeaderText("Remove confirmation");
        alert.setContentText("Are you sure you want to remove this part?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            associatedParts.remove(associatedPartsTable.getSelectionModel().getSelectedItem());
            associatedPartsTable.setItems(associatedParts);
            associatedPartsSearchField.setText("");
        } else {
            associatedPartsTable.setItems(associatedParts);
            associatedPartsSearchField.setText("");
            removePartError.setText("Part was not removed.");
        }
    }

    /**
     * Listener for the create product button. Validates input and, if validated, adds product to the inventory. If validation fails, it displays the error messages.
     */
    public void createProductButtonListener () {
        removePartError.setText("");
        Random r = new Random();
        int id = Math.abs(r.nextInt() / 100000);
        if (Integer.parseInt(minField.getText()) > Integer.parseInt(maxField.getText()) || nameField.getText().isEmpty() || Integer.parseInt(invField.getText()) > Integer.parseInt(maxField.getText()) || Integer.parseInt(invField.getText()) < Integer.parseInt(minField.getText())) {
            displayErrors();
        } else {
            try {
                Product temp = new Product(id, nameField.getText(), Double.parseDouble(priceField.getText()), Integer.parseInt(invField.getText()), Integer.parseInt(maxField.getText()), Integer.parseInt(minField.getText()));
                if (!associatedParts.isEmpty()) {
                    for (int i = 0; i < associatedParts.size(); i++) {
                        temp.addAssociatedPart(associatedParts.get(i));
                    }
                }
                Inventory.addProduct(temp);
                Stage stage = (Stage) createProductButton.getScene().getWindow();
                stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
                stage.close();
            } catch (NumberFormatException e) {
                displayErrors();
            }
        }
    }

    /**
     * Listener for the cancel button. Closes the window and redirects user to the main window.
     */
    public void cancelButtonListener () {
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
        stage.close();
    }

    /**
     * @param input the string that is being validated
     * @return Boolean value. True if it is an integer. False if it is not.
     * Checks to see if the passed string is an Integer
     */
    private boolean isInt (String input) {
        char[] charArry = input.toCharArray();
        for (int i = 0; i < charArry.length; i++) {
            if (!Character.isDigit(charArry[i])) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param input the string that is being validated
     * @return Boolean value. True if it is a double. False if it is not.
     * Checks to see if the passed string is a double.
     */
    private boolean isDouble (String input) {
        char[] charArry = input.toCharArray();
        for (int i = 0; i < charArry.length; i++) {
            if (!Character.isDigit(charArry[i]) && charArry[i] != '.') {
                return false;
            }
        }
        return true;
    }

    /**
     * Method to set error text if input fails validation.
     */
    private void displayErrors () {
        if (nameField.getText().equals("")) {
            nameError.setText("Name is empty. A value must be entered.");
        } else if (!nameField.getText().equals("")) {
            nameError.setText("");
        }
        if (priceField.getText().equals("")) {
            priceError.setText("Price is empty. A value must be entered.");
        } else if (!isDouble(priceField.getText())) {
            priceError.setText("Price must be a number");
        } else if (!priceField.getText().equals("")) {
            priceError.setText("");
        }
        if (invField.getText().equals("")) {
            invError.setText("Inv is empty. A value must be entered.");
        } else if (!isInt(invField.getText())) {
            invError.setText("Inv must be a number.");
        } else if (Integer.parseInt(invField.getText()) > Integer.parseInt(maxField.getText()) || Integer.parseInt(invField.getText()) < Integer.parseInt(minField.getText())){
            invError.setText("Inv must be between min and max.");
        } else if (!invField.getText().equals("")) {
            invError.setText("");
        }
        if (minField.getText().equals("") || maxField.getText().equals("")) {
            minMaxError.setText("Min and/or Max are empty. A value must be entered.");
        } else if (!isInt(maxField.getText()) || !isInt(minField.getText())) {
            minMaxError.setText("Min and Max must be a number.");
        } else if (Integer.parseInt(minField.getText()) > Integer.parseInt(maxField.getText())) {
            minMaxError.setText("Min cannot be greater than Max.");
        } else if (!minField.getText().equals("") && !maxField.getText().equals("")) {
            minMaxError.setText("");
        }
    }
}
