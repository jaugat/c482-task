package app;

/**
 * Supplied class Inventory.java
 */

/**
 *
 * @author Jordan Augat
 */

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.Locale;

public class Inventory {

    private static ObservableList<Part> allParts = FXCollections.observableArrayList();
    private static ObservableList<Product> allProducts = FXCollections.observableArrayList();

    /**
     * @param part the part to add
     */
    public static void addPart(Part part) {
        allParts.add(part);
    }

    /**
     * @param product the product to add
     */
    public static void addProduct(Product product) {
        allProducts.add(product);
    }

    /**
     * @param partId the part id to search for
     * @return the part with matching id. Returns null if no match is found.
     */
    public static Part lookupPart(int partId) {

        for (Part part : allParts) {
            if (partId == part.getId()) {
                return part;
            }
        }
        // show error message stating part not found
        return null;
    }

    /**
     * @param productId the product id to search for
     * @return the product with matching id. Returns null if no match is found.
     */
    public static Product lookupProduct(int productId) {
        for (Product product : allProducts) {
            if (productId == product.getId()) {
                return product;
            }
        }

        return null;
    }

    /**
     * @param name the part name to look for
     * @return the part with matching name. Returns null if no match is found.
     */
    public static Part lookupPart(String name) {
        for (Part part : allParts) {
            if (part.getName().toLowerCase().contains(name.toLowerCase())) {
                return part;
            }
        }

        return null;
    }

    /**
     * @param name the product name to look for
     * @return the product with matching name. Returns null if no match is found.
     */
    public static Product lookupProduct(String name) {
        for (Product product : allProducts) {
            if (product.getName().toLowerCase().contains(name.toLowerCase())) {
                return product;
            }
        }

        return null;
    }

    /**
     * @param index        the index of the part to update
     * @param selectedPart the part that was selected to update
     */
    public static void updatePart(int index, Part selectedPart) {

        if (selectedPart instanceof InHousePart) {
            InHousePart part = (InHousePart) allParts.get(index);
            part.setName(selectedPart.getName());
            part.setInv(selectedPart.getInv());
            part.setPrice(selectedPart.getPrice());
            part.setMax(selectedPart.getMax());
            part.setMin(selectedPart.getMin());
            part.setMachineId(((InHousePart) selectedPart).getMachineId());
        } else if (selectedPart instanceof OutsourcedPart) {
            OutsourcedPart part = (OutsourcedPart) allParts.get(index);
            part.setName(selectedPart.getName());
            part.setInv(selectedPart.getInv());
            part.setPrice(selectedPart.getPrice());
            part.setMax(selectedPart.getMax());
            part.setMin(selectedPart.getMin());
            part.setCompanyName(((OutsourcedPart) selectedPart).getCompanyName());
        }

    }

    /**
     * @param index           the index of the product to update
     * @param selectedProduct the product that was selected to update
     */
    public static void updateProduct(int index, Product selectedProduct) {
        Product product = allProducts.get(index);
        product.setName(selectedProduct.getName());
        product.setInv(selectedProduct.getInv());
        product.setPrice(selectedProduct.getPrice());
        product.setMax(selectedProduct.getMax());
        product.setMin(selectedProduct.getMin());
        ObservableList<Part> temp = product.getAllAssociatedParts();
        for (int i = 0; i < temp.size(); i++) {
            product.deleteAssociatePart(temp.get(i));
        }
        for (int i = 0; i < selectedProduct.getAllAssociatedParts().size(); i++) {
            product.addAssociatedPart(selectedProduct.getAllAssociatedParts().get(i));
        }
    }



    /**
     * @param selectedPart the selected part to delete
     */
    public static boolean deletePart (Part selectedPart) {
        return allParts.remove(selectedPart);
    }

    /**
     * @param selectedProduct the selected product to delete
     */
    public static boolean deleteProduct (Product selectedProduct) {
        return allProducts.remove(selectedProduct);
    }

    /**
     * @return a list of all parts in the system
     */
    public static ObservableList<Part> getAllParts () {
        return allParts;
    }

    /**
     * @return a list of all products in the system
     */
    public static ObservableList<Product> getAllProducts () {
        return allProducts;
    }
}
