package app;

/**
 * Supplied class InHousePart.java
 */

/**
 *
 * @author Jordan Augat
 */

public class InHousePart extends Part {

    private int machineId;

    public InHousePart(int id, String name, double price, int inv, int min, int max, int machineId) {
        super(id, name, price, inv, min, max);
        this.machineId = machineId;
    }

    /**
     * @return the machine ID
     */
    public int getMachineId() {
        return machineId;
    }

    /**
     * @param machineId the machine id to set
     */
    public void setMachineId(int machineId) {
        this.machineId = machineId;
    }
}
