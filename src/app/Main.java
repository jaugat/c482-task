package app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("MainWindow.fxml"));
        primaryStage.setTitle("");
        primaryStage.setScene(new Scene(root, 1200, 500));
        primaryStage.show();
    }

    /**
     * Javadoc files are located at the root level of the C482 task folder in their own folder titled "Javadoc"
     */
    public static void main(String[] args) {
        launch(args);
    }
}
