package app;

/**
 * Supplied class OutsourcedPart.java
 */

/**
 *
 * @author Jordan Augat
 */

public class OutsourcedPart extends Part {

    private String companyName;

    public OutsourcedPart(int id, String name, double price, int inv, int min, int max, String companyName) {
        super(id, name, price, inv, min, max);
        this.companyName = companyName;
    }

    /**
     * @return the company name
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * @param companyName the company name to set
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
}
