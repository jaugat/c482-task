package app;

/**
 * Supplied class MainWindowController.java
 */

/**
 * FUTURE ENHANCEMENT A potential enhancement for a future release would be an "Inspect part" and "Inspect product" view. This view would allow the user to see more detail on the item than what is shown in the part and product tables. A user would select an item from either table, click an "inspect" or "view part/product" button and a window would appear showing all information about the selected part or product without having to go into the modify part/product view.
 */

/**
 *
 * @author Jordan Augat
 */

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;


import java.io.IOException;
import java.util.Optional;

public class MainWindowController {

    @FXML
    private TextField partSearchField;

    @FXML
    private Label deletePartError;

    @FXML
    private Button addPartButton;

    @FXML
    private Button modifyPartButton;

    @FXML
    private Button deletePartButton;

    @FXML
    private TextField productSearchField;

    @FXML
    private Label deleteProductError;

    @FXML
    private Button addProductButton;

    @FXML
    private Button modifyProductButton;

    @FXML
    private Button deleteProductButton;

    @FXML
    private Button exitButton;

    @FXML
    private TableView<Part> partTable;

    @FXML
    private TableColumn<Part, Integer> partIdColumn;

    @FXML
    private TableColumn<Part, String> partNameColumn;

    @FXML
    private TableColumn<Part, Integer> partInvColumn;

    @FXML
    private TableColumn<Part, Double> partPriceColumn;

    @FXML
    private TableView<Product> productTable;

    @FXML
    private TableColumn<Product, Integer> productIdColumn;

    @FXML
    private TableColumn<Product, String> productNameColumn;

    @FXML
    private TableColumn<Product, Integer> productInvColumn;

    @FXML
    private TableColumn<Product, Double> productPriceColumn;

    public static Part selectedPart;
    public static Product selectedProduct;


    /**
     * Formats both parts and product tables.
     */
    public void initialize() {
        Inventory.addPart(new OutsourcedPart(10891, "MAF Sensor", 49.99, 10, 1, 20, "APR"));
        Inventory.addPart(new OutsourcedPart(60248, "Fuel Sensor", 129.99, 10, 1, 20, "Weistec"));
        Inventory.addPart(new OutsourcedPart(31915, "Air Filter", 24.99, 10, 1, 20, "MB"));

        Inventory.addProduct(new Product(98157, "Bike", 249.99, 5, 10, 1));
        Inventory.addProduct(new Product(12345, "Snowmobile", 15999.99, 2, 5, 1));
        Inventory.addProduct(new Product(35512, "ATV", 7999.99, 1, 10, 1));

        partIdColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        partNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        partInvColumn.setCellValueFactory(new PropertyValueFactory<>("inv"));
        partPriceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));
        productIdColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        productNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        productInvColumn.setCellValueFactory(new PropertyValueFactory<>("inv"));
        productPriceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));
        partTable.getItems().setAll(Inventory.getAllParts());
        productTable.getItems().setAll(Inventory.getAllProducts());
    }

    /**
     * Listener for the parts search field. Filters matching parts or returns an error if no parts are found.
     */
    public void partSearchListener() {
        String str = partSearchField.getText();
        deletePartError.setText("");
        deleteProductError.setText("");
        ObservableList<Part> temp = FXCollections.observableArrayList();
        if (isInt(str)) {
            for (int i = 0; i < Inventory.getAllParts().size(); i++) {
                if (String.valueOf(Inventory.getAllParts().get(i).getId()).contains(str)) {
                    temp.add(Inventory.getAllParts().get(i));
                }
            }
        } else {
            for (int i = 0; i < Inventory.getAllParts().size(); i++) {
                if ((Inventory.getAllParts().get(i).getName().toLowerCase()).contains(str.toLowerCase())) {
                    temp.add(Inventory.getAllParts().get(i));
                }
            }
        }
        if (temp.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("");
            alert.setHeaderText("Search Error");
            alert.setContentText("No parts found matching input!");

            alert.showAndWait();
        } else {
            partTable.setItems(temp);
        }
    }

    /**
     * Listener for the add part button. Opens add part window where user can create a new part.
     */
    public void addPartButtonListener() {
        deletePartError.setText("");
        deleteProductError.setText("");
        try {
            Parent layout = FXMLLoader.load(getClass().getResource("AddPartWindow.fxml"));
            Stage window = new Stage();
            window.initModality(Modality.APPLICATION_MODAL);
            window.setTitle("");
            window.setScene(new Scene(layout));
            window.setOnCloseRequest(e -> partTable.setItems(Inventory.getAllParts()));
            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Listener for the modify part button. Opens new window where member can modify selected part from parts table.
     */
    public void modifyPartButtonListener() {
        deletePartError.setText("");
        deleteProductError.setText("");
        selectedPart = partTable.getSelectionModel().getSelectedItem();
        if (selectedPart == null) {
            deletePartError.setText("No part selected.");
        } else {
            try {
                Parent layout = FXMLLoader.load(getClass().getResource("ModifyPartWindow.fxml"));
                Stage window = new Stage();
                window.initModality(Modality.APPLICATION_MODAL);
                window.setTitle("");
                window.setScene(new Scene(layout));
                window.setOnCloseRequest(e -> partTable.setItems(Inventory.getAllParts()));
                window.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Listener for the delete part button. Opens dialog asking user to confirm they want to delete the part. If confirmed, part is deleted from the inventory. If declined, a message appears stating part wasn't deleted.
     */
    public void deletePartButtonListener() {
        deleteProductError.setText("");
        selectedPart = partTable.getSelectionModel().getSelectedItem();
        if (selectedPart == null) {
            deletePartError.setText("No part selected.");
        } else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("");
            alert.setHeaderText("Delete confirmation");
            alert.setContentText("Are you sure you want to delete this part?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                Inventory.deletePart(partTable.getSelectionModel().getSelectedItem());
                partTable.setItems(Inventory.getAllParts());
                partSearchField.setText("");
                deletePartError.setText("");
            } else {
                partTable.setItems(Inventory.getAllParts());
                partSearchField.setText("");
                deletePartError.setText("Part was not deleted");
            }
        }
    }

    /**
     * Listener for the product search field. Filters matching parts or returns an error if no products are found.
     */
    public void productSearchListener() {
        String str = productSearchField.getText();
        deletePartError.setText("");
        deleteProductError.setText("");
        ObservableList<Product> temp = FXCollections.observableArrayList();
        if (isInt(str)) {
            for (int i = 0; i < Inventory.getAllProducts().size(); i++) {
                if (String.valueOf(Inventory.getAllProducts().get(i).getId()).contains(str)) {
                    temp.add(Inventory.getAllProducts().get(i));
                }
            }
        } else {
            for (int i = 0; i < Inventory.getAllProducts().size(); i++) {
                if ((Inventory.getAllProducts().get(i).getName().toLowerCase()).contains(str.toLowerCase())) {
                    temp.add(Inventory.getAllProducts().get(i));
                }
            }
        }
        if (temp.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("");
            alert.setHeaderText("Search Error");
            alert.setContentText("No products found matching input!");

            alert.showAndWait();
        } else {
            productTable.setItems(temp);
        }
    }

    /**
     * Listener for the add product button. Opens add part window where user can create a new product.
     */
    public void addProductButtonListener() {
        deletePartError.setText("");
        deleteProductError.setText("");
        try {
            Parent layout = FXMLLoader.load(getClass().getResource("AddProductWindow.fxml"));
            Stage window = new Stage();
            window.initModality(Modality.APPLICATION_MODAL);
            window.setTitle("");
            window.setScene(new Scene(layout));
            window.setOnCloseRequest(e -> productTable.setItems(Inventory.getAllProducts()));
            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Listener for the modify product button. Opens new window where member can modify selected product from products table.
     */
    public void modifyProductButtonListener() {
        deletePartError.setText("");
        deleteProductError.setText("");
        selectedProduct = productTable.getSelectionModel().getSelectedItem();
        if (selectedProduct == null) {
            deleteProductError.setText("No product selected.");
        } else {
            try {
                Parent layout = FXMLLoader.load(getClass().getResource("ModifyProductWindow.fxml"));
                Stage window = new Stage();
                window.initModality(Modality.APPLICATION_MODAL);
                window.setTitle("");
                window.setScene(new Scene(layout));
                window.setOnCloseRequest(e -> productTable.setItems(Inventory.getAllProducts()));
                window.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Listener for the delete product button. Shows error if product has associated parts. Otherwise, opens dialog asking user to confirm they want to delete the product. If confirmed, product is deleted from the inventory. If declined, a message appears stating product wasn't deleted.
     */
    public void deleteProductButtonListener() {
        deletePartError.setText("");
        if (selectedProduct == null) {
            deleteProductError.setText("No product selected");
        } else if (!productTable.getSelectionModel().getSelectedItem().getAllAssociatedParts().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("");
            alert.setHeaderText("Delete error");
            alert.setContentText("Product has associated parts! Unable to delete!");

            alert.showAndWait();
        } else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("");
            alert.setHeaderText("Delete confirmation");
            alert.setContentText("Are you sure you want to delete this product?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                Inventory.deleteProduct(productTable.getSelectionModel().getSelectedItem());
                productTable.setItems(Inventory.getAllProducts());
                productSearchField.setText("");
            } else {
                productTable.setItems(Inventory.getAllProducts());
                deleteProductError.setText("Product was not deleted");
            }
        }
    }

    /**
     * Listener for the exit button. Closes window and exits the application.
     */
    public void exitButtonListener() {
        System.exit(0);
    }

    /**
     * @param input the string that is being validated
     * @return Boolean value. True if it is an integer. False if it is not.
     * Checks to see if the passed string is an Integer
     */
    private boolean isInt(String input) {
        char[] charArry = input.toCharArray();
        for (int i = 0; i < charArry.length; i++) {
            if (!Character.isDigit(charArry[i])) {
                return false;
            }
        }
        return true;
    }
}
