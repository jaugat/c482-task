package app;

/**
 * Supplied class ModifyPartWindowController.java
 */

/**
 *
 * @author Jordan Augat
 */

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class ModifyPartWindowController {

    @FXML
    private RadioButton inHouseRadioButton;

    @FXML
    private RadioButton outsourcedRadioButton;

    @FXML
    private TextField idField;

    @FXML
    private TextField nameField;

    @FXML
    private TextField invField;

    @FXML
    private TextField priceField;

    @FXML
    private TextField maxField;

    @FXML
    private TextField minField;

    @FXML
    private TextField machineIdField;

    @FXML
    private Button saveButton;

    @FXML
    private Button cancelButton;

    @FXML
    private Label machineIdLabel;

    @FXML
    private Label nameError;

    @FXML
    private Label invError;

    @FXML
    private Label priceError;

    @FXML
    private Label minMaxError;

    @FXML
    private Label machineIdError;

    /**
     * Passes data from selected part on main window to the text fields.
     */
    public void initialize () {
        idField.setText(String.valueOf(MainWindowController.selectedPart.getId()));
        nameField.setText(MainWindowController.selectedPart.getName());
        invField.setText(String.valueOf(MainWindowController.selectedPart.getInv()));
        priceField.setText(String.valueOf(MainWindowController.selectedPart.getPrice()));
        minField.setText(String.valueOf(MainWindowController.selectedPart.getMin()));
        maxField.setText(String.valueOf(MainWindowController.selectedPart.getMax()));

        if (MainWindowController.selectedPart instanceof InHousePart) {
            inHouseRadioButton.setSelected(true);
            machineIdLabel.setText("Machine ID");
            machineIdField.setText(String.valueOf(((InHousePart) MainWindowController.selectedPart).getMachineId()));
        } else if (MainWindowController.selectedPart instanceof OutsourcedPart) {
            outsourcedRadioButton.setSelected(true);
            machineIdLabel.setText("Company");
            machineIdField.setText(((OutsourcedPart) MainWindowController.selectedPart).getCompanyName());
        }
    }

    /**
     * Listener for the inHouseRadioButton. Sets label text for machine ID or company field.
     */
    public void inHouseRadioButtonListener () {
        if (inHouseRadioButton.isSelected()) {
            machineIdLabel.setText("Machine ID");
        } else if (outsourcedRadioButton.isSelected()) {
            machineIdLabel.setText("Company");
        }
    }

    /**
     * Listener for the outsourcedRadioButton. Sets label text for machine ID or company field.
     */
    public void outsourcedRadioButtonListener () {
        if (inHouseRadioButton.isSelected()) {
            machineIdLabel.setText("Machine ID");
        } else if (outsourcedRadioButton.isSelected()) {
            machineIdLabel.setText("Company");
        }
    }

    /**
     * Listener for the save button. Checks if in house or outsourced is selected, validates input, and saves part if validated or shows errors if validation fails.
     */
    public void saveButtonListener () {
        if (inHouseRadioButton.isSelected()) {
            if (Integer.parseInt(minField.getText()) > Integer.parseInt(maxField.getText()) || nameField.getText().isEmpty() || Integer.parseInt(invField.getText()) > Integer.parseInt(maxField.getText()) || Integer.parseInt(invField.getText()) < Integer.parseInt(minField.getText())) {
                displayErrors();
            } else {
                try {
                    Inventory.deletePart(MainWindowController.selectedPart);
                    Inventory.addPart(new InHousePart(Integer.parseInt(idField.getText()), nameField.getText(), Double.parseDouble(priceField.getText()), Integer.parseInt(invField.getText()), Integer.parseInt(minField.getText()), Integer.parseInt(maxField.getText()), Integer.parseInt(machineIdField.getText())));
                    Stage stage = (Stage) saveButton.getScene().getWindow();
                    stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
                    stage.close();
                } catch (NumberFormatException e) {
                    displayErrors();
                }
            }
        } else if (outsourcedRadioButton.isSelected()) {
            if (Integer.parseInt(minField.getText()) > Integer.parseInt(maxField.getText()) || nameField.getText().isEmpty() || Integer.parseInt(invField.getText()) > Integer.parseInt(maxField.getText()) || Integer.parseInt(invField.getText()) < Integer.parseInt(minField.getText())) {
                displayErrors();
            } else {
                try {
                    Inventory.deletePart(MainWindowController.selectedPart);
                    Inventory.addPart(new OutsourcedPart(Integer.parseInt(idField.getText()), nameField.getText(), Double.parseDouble(priceField.getText()), Integer.parseInt(invField.getText()), Integer.parseInt(minField.getText()), Integer.parseInt(maxField.getText()), machineIdField.getText()));
                    Stage stage = (Stage) saveButton.getScene().getWindow();
                    stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
                    stage.close();
                } catch (NumberFormatException e) {
                    displayErrors();
                }
            }
        }
    }

    /**
     * Listener for the cancel button. Closes window and redirects to main window.
     */
    public void cancelButtonListener () {
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
        stage.close();
    }

    /**
     * @param input the string that is being validated
     * @return Boolean value. True if it is an integer. False if it is not.
     * Checks to see if the passed string is an Integer
     */
    private boolean isInt (String input) {
        char[] charArry = input.toCharArray();
        for (int i = 0; i < charArry.length; i++) {
            if (!Character.isDigit(charArry[i])) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param input the string that is being validated
     * @return Boolean value. True if it is a double. False if it is not.
     * Checks to see if the passed string is a double.
     */
    private boolean isDouble (String input) {
        char[] charArry = input.toCharArray();
        for (int i = 0; i < charArry.length; i++) {
            if (!Character.isDigit(charArry[i]) && charArry[i] != '.') {
                return false;
            }
        }
        return true;
    }

    /**
     * Method to set error text if input fails validation.
     */
    private void displayErrors () {
        if (nameField.getText().equals("")) {
            nameError.setText("Name is empty. A value must be entered.");
        } else if (!nameField.getText().equals("")) {
            nameError.setText("");
        }
        if (priceField.getText().equals("")) {
            priceError.setText("Price is empty. A value must be entered.");
        } else if (!isDouble(priceField.getText())) {
            priceError.setText("Price must be a number");
        } else if (!priceField.getText().equals("")) {
            priceError.setText("");
        }
        if (invField.getText().equals("")) {
            invError.setText("Inv is empty. A value must be entered.");
        } else if (!isInt(invField.getText())) {
            invError.setText("Inv must be a number.");
        } else if (Integer.parseInt(invField.getText()) > Integer.parseInt(maxField.getText()) || Integer.parseInt(invField.getText()) < Integer.parseInt(minField.getText())){
            invError.setText("Inv must be between min and max.");
        } else if (!invField.getText().equals("")) {
            invError.setText("");
        }
        if (minField.getText().equals("") || maxField.getText().equals("")) {
            minMaxError.setText("Min and/or Max are empty. A value must be entered.");
        } else if (!isInt(maxField.getText()) || !isInt(minField.getText())) {
            minMaxError.setText("Min and Max must be a number.");
        } else if (Integer.parseInt(minField.getText()) > Integer.parseInt(maxField.getText())) {
            minMaxError.setText("Min cannot be greater than Max.");
        } else if (!minField.getText().equals("") && !maxField.getText().equals("")) {
            minMaxError.setText("");
        }
        if (machineIdField.getText().equals("")) {
            if (inHouseRadioButton.isSelected()) {
                machineIdError.setText("Machine ID is empty. A value must be entered.");
            } else if (outsourcedRadioButton.isSelected()) {
                machineIdError.setText("Company is empty. A value must be entered.");
            }
        } else if (inHouseRadioButton.isSelected() && !isInt(machineIdField.getText())) {
            machineIdError.setText("Machine ID must be a number");
        } else if (!machineIdField.getText().equals("")) {
            machineIdError.setText("");
        }
    }
}
