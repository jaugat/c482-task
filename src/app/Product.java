package app;

/**
 * Supplied class Product.java
 */

/**
 *
 * @author Jordan Augat
 */

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Product {

    private ObservableList<Part> associatedParts;
    private int id;
    private String name;
    private double price;
    private int inv;
    private int max;
    private int min;

    public Product(int id, String name, double price, int inv, int max, int min) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.inv = inv;
        this.max = max;
        this.min = min;
        associatedParts = FXCollections.observableArrayList();
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * @return the inventory level
     */
    public int getInv() {
        return inv;
    }

    /**
     * @param inv the inventory to set
     */
    public void setInv(int inv) {
        this.inv = inv;
    }

    /**
     * @return the max
     */
    public int getMax() {
        return max;
    }

    /**
     * @param max the max to set
     */
    public void setMax(int max) {
        this.max = max;
    }

    /**
     * @return the min
     */
    public int getMin() {
        return min;
    }

    /**
     * @param min the min to set
     */
    public void setMin(int min) {
        this.min = min;
    }

    /**
     * @param part the part to add
     */
    public void addAssociatedPart (Part part) {
        associatedParts.add(part);
    }

    /**
     * @param selectedPart the part to delete
     */
    public boolean deleteAssociatePart (Part selectedPart) {
        return associatedParts.remove(selectedPart);
    }

    /**
     * @return the list of all associated parts
     */
    public ObservableList<Part> getAllAssociatedParts () {
        return associatedParts;
    }
}
